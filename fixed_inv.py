import random
#attempts = input('Attempts:')
#att = int(attempts)
att = 6105
bet = 1
coef = []
tests = 1000
res_win = 0
res_loss = 0
w = 0
l = 0
for t in range(0, tests):
	for i in range(0, att):
		coef.append(random.random())
	res = - att*bet
	for i in range(0, att):
		if(random.random() < coef[i]):
			res += bet/coef[i]
	if res > 0:
		w+=1
		res_win += res
	else:
		l+=1
		res_loss += res
print('Wins: ', (w/tests)*100, '% Loses: ', (l/tests)*100, '%\n')
print('Average win: ', res_win/w, '\n')
print('Average loss: ', res_loss/l, '\n')

