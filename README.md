# README #

This is an attempt to simulate different gambling strategies. 

Description of included files:  
calc.py  
	Betting random amounts of money on random odds  
fixed_inv.py  
	Betting a fixed amount of money on random odds  
increasing_inv.py  
	Betting linearly increasing amounts of money on fixed odds  

Contact Katerina Petelova at katerinapetelova@gmail.com