import random
#attempts = input('Attempts:')
#att = int(attempts)
att = 120
max_inv = 120
coef = []
bet = []
tests = 10000
res_win = 0
res_loss = 0
w = 0
l = 0
total = 0
for t in range(0, tests):
	res = 0
	for i in range(0, att):
		coef.append(random.random())
		bet.append(random.randint(1, max_inv)*10)
		res -= bet[i]
	total -= res
	for i in range(0, att):
		if(random.random() < coef[i]):
			res += bet[i]/coef[i]
	if res > 0:
		w+=1
		res_win += res
	else:
		l+=1
		res_loss += res
print('Wins: ', (w/tests)*100, '% Loses: ', (l/tests)*100, '%\n')
print('Average win: ', res_win/w, 'Balance: ', res_win/w + total/tests,'\n')
print('Average loss: ', res_loss/l, 'Balance: ', res_loss/l + total/tests,'\n')
print('Average investment: ', total/tests, '\n')
